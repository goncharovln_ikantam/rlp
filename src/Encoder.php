<?php

namespace Goncharovln\Rlp;

use Goncharovln\Rlp\Factories\BufferFactory;

/**
 * Class Encoder
 * @package Goncharovln\Rlp
 */
class Encoder
{
    /**
     * @var BufferFactory
     */
    protected $bufferFactory;

    /**
     * @var DataConverter
     */
    protected $dataConverter;

    /**
     * Encoder constructor.
     */
    public function __construct()
    {
        $this->bufferFactory = new BufferFactory();
        $this->dataConverter = new DataConverter();
    }

    /**
     * @param $inputs
     * @return Buffer
     */
    public function encode($inputs)
    {
        if (is_array($inputs)) {
            return $this->encodeArray($inputs);
        }

        return $this->encodeInputs($inputs);
    }

    /**
     * @param $inputs
     * @return Buffer
     */
    protected function encodeInputs($inputs)
    {
        $output = $this->bufferFactory->create();
        $input = $this->bufferFactory->create($inputs);
        $length = $input->length();

        if ($length === 1 && $input[0] < 128) {
            return $input;
        } else {
            return $output->concat($this->encodeLength($length, 128), $input);
        }
    }

    /**
     * @param array $inputs
     * @return Buffer
     */
    protected function encodeArray(array $inputs)
    {
        $output = $this->bufferFactory->create();
        $result = $this->bufferFactory->create();

        foreach ($inputs as $input) {
            $output->concat($this->encode($input));
        }

        return $result->concat($this->encodeLength($output->length(), 192), $output);
    }

    /**
     * @param int $length
     * @param int $offset
     * @return Buffer
     */
    protected function encodeLength(int $length, int $offset)
    {
        if ($length < 56) {
            return $this->bufferFactory->createRaw(strval($length + $offset));
        }
        $hexLength = $this->dataConverter->intToHex($length);
        $firstByte = $this->dataConverter->intToHex($offset + 55 + (strlen($hexLength) / 2));

        return $this->bufferFactory->create('0x' . strval($firstByte . $hexLength), 'hex');
    }
}
