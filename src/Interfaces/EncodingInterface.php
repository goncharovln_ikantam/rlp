<?php

namespace Goncharovln\Rlp\Interfaces;

/**
 * Interface EncodingInterface
 * @package Goncharovln\Rlp\Interfaces
 */
interface EncodingInterface
{
    /**
     * @param string $input
     * @return array
     */
    public function stringToData(string $input): array;

    /**
     * @param array $inputs
     * @return string
     */
    public function toString(array $inputs);
}
