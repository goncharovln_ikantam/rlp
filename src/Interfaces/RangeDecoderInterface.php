<?php

namespace Goncharovln\Rlp\Interfaces;

use Goncharovln\Rlp\Buffer;
use Goncharovln\Rlp\Decoder\DecodeBuffer;

/**
 * Interface RangeDecoderInterface
 * @package Goncharovln\Rlp\Interfaces
 */
interface RangeDecoderInterface
{
    /**
     * return decoded Buffer
     *
     * @param Buffer $input
     * @return DecodeBuffer
     */
    public function getDecodeBuffer(Buffer $input);
}
