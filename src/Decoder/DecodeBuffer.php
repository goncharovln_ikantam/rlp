<?php

namespace Goncharovln\Rlp\Decoder;

use Goncharovln\Rlp\Buffer;

/**
 * Class DecodeBuffer
 * @package Goncharovln\Rlp\Decoder
 */
class DecodeBuffer
{
    /**
     * @var mixed
     */
    private $decodedBuffers;

    /**
     * @var Buffer
     */
    private $remainder;

    /**
     * DecodeBuffer constructor.
     * @param [] Buffer|Buffer $decodedBuffers
     * @param Buffer $remainder
     */
    public function __construct($decodedBuffers, Buffer $remainder)
    {
        $this->decodedBuffers = $decodedBuffers;
        $this->remainder = $remainder;
    }

    /**
     * @return Buffer
     */
    public function getDecodedBuffers()
    {
        return $this->decodedBuffers;
    }

    /**
     * @return Buffer
     */
    public function getReminder()
    {
        return $this->remainder;
    }
}
