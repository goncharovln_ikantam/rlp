<?php

namespace Goncharovln\Rlp\Decoder\Ranges;

use Goncharovln\Rlp\Buffer;
use Goncharovln\Rlp\Interfaces\RangeDecoderInterface;
use Goncharovln\Rlp\Decoder\DecodeBuffer;

/**
 * Class ThirdRange
 * @package Goncharovln\Rlp\Decoder\Ranges
 */
class ThirdRange implements RangeDecoderInterface
{
    /**
     * @param Buffer $input
     * @return DecodeBuffer
     */
    public function getDecodeBuffer(Buffer $input)
    {
        $firstByte = $input[0];
        $llength = $firstByte - 0xb6;
        $hexLength = $input->slice(1, $llength)->toString('hex');

        if ($hexLength === '00') {
            throw new \RuntimeException('Invalid hex length.');
        }
        $length = hexdec($hexLength);
        $data = $input->slice($llength, $length + $llength);

        if ($data->length() < $length) {
            throw new \RuntimeException('Invalid hex length.');
        }

        return new DecodeBuffer($data, $input->slice($length + $llength));
    }
}
