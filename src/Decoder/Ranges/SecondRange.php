<?php

namespace Goncharovln\Rlp\Decoder\Ranges;

use Goncharovln\Rlp\Factories\BufferFactory;
use Goncharovln\Rlp\Buffer;
use Goncharovln\Rlp\Decoder\DecodeBuffer;
use Goncharovln\Rlp\Interfaces\RangeDecoderInterface;

/**
 * Class SecondRange
 * @package Goncharovln\Rlp\Decoder\Ranges
 */
class SecondRange implements RangeDecoderInterface
{
    /**
     * @var BufferFactory
     */
    protected $bufferFactory;

    /**
     * SecondRange constructor.
     * @param BufferFactory $bufferFactory
     */
    public function __construct(BufferFactory $bufferFactory)
    {
        $this->bufferFactory = $bufferFactory;
    }

    /**
     * @param Buffer $input
     * @return DecodeBuffer
     */
    public function getDecodeBuffer(Buffer $input)
    {
        $firstByte = $input[0];
        $length = $firstByte - 0x7f;
        $data = $this->bufferFactory->create();

        if ($firstByte !== 0x80) {
            $data = $input->slice(1, $length);
        }
        if ($length === 2 && $data[0] < 0x80) {
            throw new \RuntimeException('Byte must be less than 0x80.');
        }

        return new DecodeBuffer($data, $input->slice($length));
    }
}
