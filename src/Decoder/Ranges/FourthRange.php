<?php

namespace Goncharovln\Rlp\Decoder\Ranges;

use Goncharovln\Rlp\Decoder;
use Goncharovln\Rlp\Buffer;
use Goncharovln\Rlp\Interfaces\RangeDecoderInterface;
use Goncharovln\Rlp\Decoder\DecodeBuffer;

/**
 * Class FourthRange
 * @package Goncharovln\Rlp\Decoder\Ranges
 */
class FourthRange implements RangeDecoderInterface
{
    /**
     * @var Decoder
     */
    protected $decoder;

    /**
     * FourthRange constructor.
     * @param Decoder $decoder
     */
    public function __construct(Decoder $decoder)
    {
        $this->decoder = $decoder;
    }

    /**
     * @param Buffer $input
     * @return DecodeBuffer
     */
    public function getDecodeBuffer(Buffer $input)
    {
        $firstByte = $input[0];
        $length = $firstByte - 0xbf;
        $innerRemainder = $input->slice(1, $length);
        $decoded = [];

        while ($innerRemainder->length()) {
            $decodeBuffer = $this->decoder->decodeData($innerRemainder);
            $decoded[] = $decodeBuffer->getDecodedBuffers();
            $innerRemainder = $decodeBuffer->getReminder();
        }

        return new DecodeBuffer($decoded, $input->slice($length));
    }
}
