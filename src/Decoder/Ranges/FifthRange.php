<?php

namespace Goncharovln\Rlp\Decoder\Ranges;

use Goncharovln\Rlp\Decoder;
use Goncharovln\Rlp\Decoder\DecodeBuffer;
use Goncharovln\Rlp\Buffer;
use Goncharovln\Rlp\Interfaces\RangeDecoderInterface;

/**
 * Class FifthRange
 * @package Goncharovln\Rlp\Decoder\Ranges
 */
class FifthRange implements RangeDecoderInterface
{
    /**
     * @var Decoder
     */
    protected $decoder;

    /**
     * FourthRange constructor.
     * @param Decoder $decoder
     */
    public function __construct(Decoder $decoder)
    {
        $this->decoder = $decoder;
    }

    /**
     * @param Buffer $input
     * @return DecodeBuffer
     */
    public function getDecodeBuffer(Buffer $input)
    {
        $firstByte = $input[0];
        $llength = $firstByte - 0xf6;
        $hexLength = $input->slice(1, $llength)->toString('hex');
        $decoded = [];

        if ($hexLength === '00') {
            throw new \RuntimeException('Invalid hex length.');
        }
        $length = hexdec($hexLength);
        $totalLength = $llength + $length;

        if ($totalLength > $input->length()) {
            throw new \RuntimeException('Invalid RLP: total length is bigger than data length.');
        }
        $innerRemainder = $input->slice($llength, $totalLength);

        if ($innerRemainder->length() === 0) {
            throw new \RuntimeException('Invalid RLP: list has invalid length.');
        }

        while ($innerRemainder->length()) {
            $decodeBuffer = $this->decoder->decodeData($innerRemainder);
            $decoded[] = $decodeBuffer->getDecodedBuffers();
            $innerRemainder = $decodeBuffer->getReminder();
        }

        return new DecodeBuffer($decoded, $input->slice($length));
    }
}
