<?php

namespace Goncharovln\Rlp\Decoder\Ranges;

use Goncharovln\Rlp\Buffer;
use Goncharovln\Rlp\Interfaces\RangeDecoderInterface;
use Goncharovln\Rlp\Decoder\DecodeBuffer;

/**
 * Class FirstRange
 * @package Goncharovln\Rlp\Decoder\Ranges
 */
class FirstRange implements RangeDecoderInterface
{
    /**
     * @param Buffer $input
     * @return DecodeBuffer
     */
    public function getDecodeBuffer(Buffer $input)
    {
        return new DecodeBuffer($input->slice(0, 1), $input->slice(1));
    }
}
