<?php

namespace Goncharovln\Rlp\Factories;

use Goncharovln\Rlp\Interfaces\RangeDecoderInterface;
use Goncharovln\Rlp\Decoder\Ranges\FirstRange;
use Goncharovln\Rlp\Decoder\Ranges\SecondRange;
use Goncharovln\Rlp\Decoder\Ranges\ThirdRange;
use Goncharovln\Rlp\Decoder\Ranges\FourthRange;
use Goncharovln\Rlp\Decoder\Ranges\FifthRange;
use Goncharovln\Rlp\Decoder;

/**
 * Class DecoderRangeFactory
 * @package Goncharovln\Rlp\Factories
 */
class DecoderRangeFactory
{
    /**
     * @var RangeDecoderInterface
     */
    protected $firstRange;

    /**
     * @var RangeDecoderInterface
     */
    protected $secondRange;

    /**
     * @var RangeDecoderInterface
     */
    protected $thirdRange;

    /**
     * @var RangeDecoderInterface
     */
    protected $fourthRange;

    /**
     * @var RangeDecoderInterface
     */
    protected $fifthRange;

    /**
     * @param int $firstByte
     * @param BufferFactory $bufferFactory
     * @param Decoder $decoder
     * @return RangeDecoderInterface
     */
    public function create(int $firstByte, BufferFactory $bufferFactory, Decoder $decoder): RangeDecoderInterface
    {
        if ($firstByte <= 0x7f) {
            if (!$this->firstRange) {
                $this->firstRange = new FirstRange();
            }

            return $this->firstRange;
        } elseif ($firstByte <= 0xb7) {
            if (!$this->secondRange) {
                $this->secondRange = new SecondRange($bufferFactory);
            }

            return $this->secondRange;
        } elseif ($firstByte <= 0xbf) {
            if (!$this->thirdRange) {
                $this->thirdRange = new ThirdRange();
            }

            return $this->thirdRange;
        } elseif ($firstByte <= 0xf7) {
            if (!$this->fourthRange) {
                $this->fourthRange = new FourthRange($decoder);
            }

            return $this->fourthRange;
        } else {
            if (!$this->fifthRange) {
                $this->fifthRange = new FifthRange($decoder);
            }

            return $this->fifthRange;
        }
    }
}
