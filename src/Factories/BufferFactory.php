<?php

namespace Goncharovln\Rlp\Factories;

use Goncharovln\Rlp\Buffer;

/**
 * Class BufferFactory
 * @package Goncharovln\Rlp\Factories
 */
class BufferFactory
{
    /**
     * @var EncodingFactory
     */
    protected $encodingFactory;

    /**
     * BufferFactory constructor.
     */
    public function __construct()
    {
        $this->encodingFactory = new EncodingFactory();
    }

    /**
     * @param mixed $data
     * @param string $encoding
     * @return Buffer
     */
    public function create($data = [], string $encoding = 'utf8')
    {
        if (is_array($data)) {
            return new Buffer($this->encodingFactory, $data, $encoding);
        } elseif (is_string($data)) {
            if (strpos($data, '0x') === 0) {
                return new Buffer($this->encodingFactory, $data, 'hex');
            }
            return new Buffer($this->encodingFactory, str_split($data, 1));
        } elseif (is_numeric($data)) {
            if (!$data || $data < 0) {
                return new Buffer($this->encodingFactory);
            }
            if (is_float($data)) {
                $data = number_format($data, 0, '', '');
            }
            $gmpInput = gmp_init($data, 10);
            return new Buffer($this->encodingFactory, '0x' . gmp_strval($gmpInput, 16), 'hex');
        } elseif ($data === null) {
            return new Buffer($this->encodingFactory);
        } elseif ($data instanceof Buffer) {
            return $data;
        }

        throw new \InvalidArgumentException('The input type did not support.');
    }

    /**
     * @param $data
     * @param string $encoding
     * @return Buffer
     */
    public function createRaw($data = [], string $encoding = 'utf8')
    {
        return new Buffer($this->encodingFactory, $data, $encoding);
    }
}
