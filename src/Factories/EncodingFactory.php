<?php

namespace Goncharovln\Rlp\Factories;

use Goncharovln\Rlp\DataConverter;
use Goncharovln\Rlp\Encodings\Hex;
use Goncharovln\Rlp\Encodings\Ascii;
use Goncharovln\Rlp\Encodings\Utf8;
use Goncharovln\Rlp\Interfaces\EncodingInterface;

/**
 * Class EncodingFactory
 * @package Goncharovln\Rlp\Factories
 */
class EncodingFactory
{
    /**
     * @var Hex
     */
    private $hex;

    /**
     * @var Ascii
     */
    private $ascii;

    /**
     * @var Utf8
     */
    private $utf8;

    /**
     * @param string $encoding
     * @return EncodingInterface
     */
    public function create(string $encoding): EncodingInterface
    {
        switch ($encoding) {
            case 'hex':
                if (!$this->hex) {
                    $this->hex = new Hex(new DataConverter());
                }
                return $this->hex;
            case 'ascii':
                if (!$this->ascii) {
                    $this->ascii = new Ascii(new DataConverter());
                }
                return $this->ascii;
            case 'utf8':
                if (!$this->utf8) {
                    $this->utf8 = new Utf8(new DataConverter());
                }
                return $this->utf8;
            default:
                throw new \InvalidArgumentException('Invalid encoding.');
                break;
        }
    }
}
