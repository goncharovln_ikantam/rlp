<?php

namespace Goncharovln\Rlp;

use ArrayAccess;
use Goncharovln\Rlp\Factories\EncodingFactory;

/**
 * Class Buffer
 * @package Goncharovln\Rlp
 */
class Buffer implements ArrayAccess
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var string
     */
    protected $encoding = 'utf8';

    /**
     * @var EncodingFactory
     */
    protected $encodingFactory;

    /**
     * Buffer constructor.
     * @param EncodingFactory $encodingFactory
     * @param array $data
     * @param string $encoding
     */
    public function __construct(EncodingFactory $encodingFactory, $data = [], string $encoding = '')
    {
        if (!empty($encoding)) {
            $this->encoding = strtolower($encoding);
        }

        $this->encodingFactory = new EncodingFactory();

        if ($data) {
            $this->data = $this->decodeToData($data);
        }
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * @param mixed $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    /**
     * @param string $encoding
     * @return string
     */
    public function toString(string $encoding = '')
    {
        $encoding = empty($encoding) ? $this->encoding : strtolower($encoding);

        return $this->encodingFactory->create($encoding)->toString($this->data);
    }

    /**
     * @return int
     */
    public function length(): int
    {
        return count($this->data);
    }

    /**
     * @param array ...$inputs
     * @return $this
     */
    public function concat(... $inputs)
    {
        foreach ($inputs as $input) {
            if (is_array($input)) {
                $input = new static($this->encodingFactory, $input);
            }

            if ($input instanceof static) {
                $length = $input->length();
                for ($i = 0; $i < $length; $i++) {
                    $this->data[] = $input[$i];
                }
            } else {
                throw new \InvalidArgumentException('Input must be array or Buffer');
            }
        }

        return $this;
    }

    /**
     * @param int $start
     * @param null $end
     * @return static
     */
    public function slice(int $start = 0, $end = null)
    {
        if ($end === null) {
            $end = $this->length();
        }
        if ($end > 0) {
            $end -= $start;
        } elseif ($end === 0) {
            return new static($this->encodingFactory);
        }
        $sliced = array_slice($this->data, $start, $end);

        return new static($this->encodingFactory, $sliced);
    }

    /**
     * @param $input
     * @return array
     */
    protected function decodeToData($input)
    {
        $output = [];

        if (is_array($input)) {
            $output = $this->arrayToData($input);
        } elseif (is_int($input)) {
            $output = $this->intToData($input);
        } elseif (is_numeric($input)) {
            $output = $this->numericToData($input);
        } elseif (is_string($input)) {
            $output = $this->stringToData($input, $this->encoding);
        }

        return $output;
    }

    /**
     * arrayToData
     *
     * @param array $inputs
     * @return array
     */
    protected function arrayToData(array $inputs)
    {
        $output = [];

        foreach ($inputs as $input) {
            if (is_string($input)) {
                $output = array_merge($output, $this->stringToData($input, $this->encoding));
            } elseif (is_numeric($input)) {
                $output = array_merge($output, $this->numericToData($input));
            } else {
                throw new \InvalidArgumentException('Invalid type');
            }
        }
        return $output;
    }

    /**
     * @param string $input
     * @param string $encoding
     * @return array
     */
    protected function stringToData(string $input, string $encoding)
    {
        return $this->encodingFactory->create($encoding)->stringToData($input);
    }

    /**
     * @param $input
     * @return array
     */
    protected function numericToData($input)
    {
        return [(int)$input];
    }

    /**
     * @param $input
     * @return array
     */
    protected function intToData($input)
    {
        return array_fill(0, $input, 0);
    }
}
