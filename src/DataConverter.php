<?php

namespace Goncharovln\Rlp;

/**
 * Class Encoder
 * @package Goncharovln\Rlp
 */
class DataConverter
{
    /**
     * @param int $value
     * @return string
     */
    public function intToHex(int $value)
    {
        $hex = dechex($value);

        return $this->padToEven($hex);
    }

    /**
     * @param string $value
     * @return string
     */
    public function padToEven(string $value)
    {
        if ((strlen($value) % 2) !== 0) {
            $value = '0' . $value;
        }
        return $value;
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function stringToHex(string $value)
    {
        if (strpos($value, '0x') === 0) {
            $value = str_replace('0x', '', $value);
        }

        return $value;
    }

    /**
     * @param string $value
     * @return string
     */
    public function stringToEven(string $value)
    {
        return $this->padToEven($this->stringToHex($value));
    }

    /**
     * @param int $value
     * @return string
     */
    public function intToChar(int $value)
    {
        return chr($value);
    }
}
