<?php

namespace Goncharovln\Rlp\Encodings;

use Goncharovln\Rlp\DataConverter;
use Goncharovln\Rlp\Interfaces\EncodingInterface;

/**
 * Class Utf8
 * @package Goncharovln\Rlp\Encodings
 */
class Utf8 implements EncodingInterface
{
    /**
     * @var DataConverter
     */
    protected $dataConverter;

    /**
     * Utf8 constructor.
     * @param DataConverter $dataConverter
     */
    public function __construct(DataConverter $dataConverter)
    {
        $this->dataConverter = new DataConverter();
    }

    /**
     * @param string $input
     * @return array
     */
    public function stringToData(string $input): array
    {
        return unpack('C*', $input);
    }

    /**
     * @param array $inputs
     * @return string
     */
    public function toString(array $inputs)
    {
        $output = '';
        foreach ($inputs as $input) {
            $output .= $this->dataConverter->intToChar($input);
        }

        return $output;
    }
}
