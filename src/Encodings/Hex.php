<?php

namespace Goncharovln\Rlp\Encodings;

use Goncharovln\Rlp\DataConverter;
use Goncharovln\Rlp\Interfaces\EncodingInterface;

/**
 * Class Hex
 * @package Goncharovln\Rlp\Encodings
 */
class Hex implements EncodingInterface
{
    /**
     * @var DataConverter
     */
    protected $dataConverter;

    /**
     * Hex constructor.
     * @param DataConverter $dataConverter
     */
    public function __construct(DataConverter $dataConverter)
    {
        $this->dataConverter = new DataConverter();
    }

    /**
     * @param string $input
     * @return array
     */
    public function stringToData(string $input): array
    {
        $input = $this->dataConverter->stringToEven($input);
        return array_map('hexdec', str_split($input, 2));
    }

    /**
     * @param array $inputs
     * @return string
     */
    public function toString(array $inputs)
    {
        $output = '';
        foreach ($inputs as $input) {
            $output .= $this->dataConverter->intToHex($input);
        }

        return $output;
    }
}
