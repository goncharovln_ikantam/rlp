<?php

namespace Goncharovln\Rlp\Encodings;

use Goncharovln\Rlp\DataConverter;
use Goncharovln\Rlp\Interfaces\EncodingInterface;

/**
 * Class Ascii
 * @package Goncharovln\Rlp\Encodings
 */
class Ascii implements EncodingInterface
{
    /**
     * @var DataConverter
     */
    protected $dataConverter;

    /**
     * Ascii constructor.
     * @param DataConverter $dataConverter
     */
    public function __construct(DataConverter $dataConverter)
    {
        $this->dataConverter = new DataConverter();
    }

    /**
     * @param string $input
     * @return array
     */
    public function stringToData(string $input): array
    {
        return array_map('ord', str_split($input, 1));
    }

    /**
     * @param array $inputs
     * @return string
     */
    public function toString(array $inputs)
    {
        $output = '';
        foreach ($inputs as $input) {
            $output .= $this->dataConverter->intToChar($input);
        }

        return $output;
    }
}
