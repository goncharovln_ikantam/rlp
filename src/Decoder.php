<?php

namespace Goncharovln\Rlp;

use Goncharovln\Rlp\Factories\BufferFactory;
use Goncharovln\Rlp\Factories\DecoderRangeFactory;
use Goncharovln\Rlp\Decoder\DecodeBuffer;

/**
 * Class Decoder
 * @package Goncharovln\Rlp
 */
class Decoder
{
    /**
     * @var BufferFactory
     */
    protected $bufferFactory;

    /**
     * @var DecoderRangeFactory
     */
    protected $decoderRangeFactory;

    /**
     * Decoder constructor.
     */
    public function __construct()
    {
        $this->bufferFactory = new BufferFactory();
        $this->decoderRangeFactory = new DecoderRangeFactory();
    }

    /**
     * @param string $input
     * @return Buffer
     */
    public function decode(string $input)
    {
        $input = $this->bufferFactory->create($input);
        $decoded = $this->decodeData($input);

        return $decoded->getDecodedBuffers();
    }

    /**
     * @param \Goncharovln\Rlp\Buffer $input
     * @return DecodeBuffer
     */
    public function decodeData(Buffer $input)
    {
        $firstByte = $input[0];

        return $this->decoderRangeFactory->create($firstByte, $this->bufferFactory, $this)->getDecodeBuffer($input);
    }
}
