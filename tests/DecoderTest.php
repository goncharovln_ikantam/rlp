<?php

namespace Test;

use PHPUnit\Framework\TestCase;
use Goncharovln\Rlp\Decoder;
use Goncharovln\Rlp\Encoder;

/**
 * Class DecoderTest
 * @package Test
 */
class DecoderTest extends TestCase
{
    /**
     * @var Decoder
     */
    protected $decoder;

    /**
     * @var Encoder
     */
    protected $encoder;

    /**
     * @return void
     */
    public function setUp()
    {
        $this->decoder = new Decoder();
        $this->encoder = new Encoder();
    }

    /**
     * @return void
     */
    public function testString()
    {
        $string = 'dog';
        $encodedBuffer = $this->encoder->encode('dog');

        $decodedBuffer = $this->decoder->decode('0x' . $encodedBuffer->toString('hex'));
        $this->assertEquals($string, $decodedBuffer->toString());
    }

    /**
     * @return void
     */
    public function testStringArray()
    {
        $stringArray = ['dog', 'cat'];

        $encodedBuffer = $this->encoder->encode($stringArray);

        $decodedBuffer = $this->decoder->decode('0x' . $encodedBuffer->toString('hex'));
        $this->assertEquals(2, count($decodedBuffer));

        foreach ($stringArray as $index => $string) {
            $this->assertEquals($string, $decodedBuffer[$index]->toString());
        }
    }

    /**
     * @return void
     */
    public function testNull()
    {
        $encodedBuffer = $this->encoder->encode(null);

        $decodedBuffer = $this->decoder->decode('0x' . $encodedBuffer->toString('hex'));
        $this->assertEquals('', $decodedBuffer->toString());
    }

    /**
     * @return void
     */
    public function testEmptyArray()
    {
        $encodedBuffer = $this->encoder->encode([]);

        $decodedBuffer = $this->decoder->decode('0x' . $encodedBuffer->toString('hex'));

        $this->assertEquals(0, count($decodedBuffer));
    }
}
