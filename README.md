# RLP
Recursive Length Prefix Encoding in PHP.

# Usage

```php
use Goncharovln\Rlp\Encoder;
use Goncharovln\Rlp\Decoder;

$rlpEncoder = new Encoder;
$encodedBuffer = $rlpEncoder->encode(['dog']);

$rlpDecoder = new Decoder;
$decodedArray = $rlpDecoder->decode('0x' . $encodedBuffer->toString('hex'));

echo $decodedArray[0]->toString();// show dog
```

# API

#### encode

Returns recursive length prefix encoding of given data.

`encode(mixed $inputs)`

Mixed inputs - array of string, integer or numeric string.

#### decode

Returns array recursive length prefix decoding of given data.

`decode(string $input)`

String input - recursive length prefix encoded string.

# License
MIT
